# sipm_pde_absolute

This project describe how to calculate the absolute PDE for SiPM device. 

## Description

Project to analyse SiPM waveforms aquired with pulsed light and use so called Poisson method to calcualte the absolute PDE.

Before using the project, it is strongly recomended to read article [Characterisation of a large area silicon photomultiplier](https://arxiv.org/pdf/1810.02275.pdf). In particular part Section #5 


## Structure
Project containes:
*   **sipm_pde_absolute.py**        -> calss to calculate absolute PDE;
*   **pde_ana_absolute.ipynd**      -> jupiter notebook with exapels how to analyse absolute PDE data and use **sipm_pde_absolute**
*   **PD_S1337-1010BQ_SN01.out**    -> QE for calibrated photodiode
*   **images**                      -> folder with some images used to discribe analysis procedure
*   **data_example**                -> filder with experimental data
*   **FbK_ratio_405nm.txt**         -> file containes the measuremts of power ratio R = Ppd/Psipm, between the light intensity measured by the calibrated photodiode **$P_{pd}$** and the SiPM **$P_{sipm}$**
*   **TLS_75X.pdf**                 -> manual for the lamp and monochromator used for measurements

 
## Usage
To use the project, the txt files with experimental data is neede. Detailed description how to use **sipm_pde_absolute** class, as well as neede input data format can be found in jupiter notebook **pde_ana_absolute**


## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.

