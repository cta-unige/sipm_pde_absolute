import ROOT as root
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import math
import scipy

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d

class PDE_Absolute:

    '''
    Class to analyse the absolute PDE measurements
    '''

    def __init__(self, trigger_range = (np.nan, np.nan), nsb_range = (np.nan, np.nan)):

        self.root_files = []
        print('class fro SiPM PDE characterization has been created')

        if any(np.isnan(trigger_range)):
            print('trigger_range == NaN')

        if any(np.isnan(nsb_range)):
            print('nsb_range == NaN')

        self.trigger_range = trigger_range
        self.nsb_range = nsb_range
        #self.baseline = np.nan
        self.baseline_amplitude = np.nan
        self.data = {}
        self.power_r = np.nan
        self.f_qe = np.nan

    def get_root_file(self, path, baseline = '50.0'):

        '''
        get all root files on a given folder.
        Usually one file with baseline measurements (Vbias < Vbd) should be inside the folder

        :param path: path to folder with root files
        :param baseline: string of Vbias for measurements with baseline (measuremetns done below Vbd)
        :return: root files at folder
        '''
        self.data_path = path
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        for ifile in onlyfiles:
            if ifile.find('.root') != -1:

                if ifile.find(baseline) != -1:
                    self.baseline_root_file = ifile
                else:
                    self.root_files.append(str(ifile))

        self.root_files = list(np.sort(np.array(self.root_files)))

    def get_baseline(self):

        '''
        calculate the baseline amplitudes
        :return: baseline amplitude
        '''

        f = root.TFile(self.data_path + self.baseline_root_file)
        myTree = f.Get("T")
        amplitude = []

        for entry_num in range(0, myTree.GetEntries()):

            myTree.GetEntry(entry_num)
            if entry_num == 0:
                self.baseline_increment = myTree.dt
                self.baseline_n_points = myTree.NPoints
                self.baseline_v_bias = myTree.Vbias

            amplitude.append(np.array(list(myTree.Volt)))

        self.baseline_amplitude = np.mean(amplitude, axis=0)

    def get_trigger_range(self, file, n_std = 5, doshow=False):

        '''
        developed to calculate the trigger (i.e. light) and dark time windows
        :param file: file to be analysed
        :param n_std: number of STD to be used for time window
        :param doshow: show or not
        :return:
        '''

        print('   start: get_trigger_range')
        print('       - file : ', file)
        print('')

        f = root.TFile(self.data_path + file)
        myTree = f.Get("T")

        n_points = 0
        increment = 0
        amplitude = []
        amplitude_arg_max = []
        amplitude_max = []
        baseline = []
        pulse_amplitude = []
        n_dcr = []
        n_light = []
        photodiode_current = []
        index_max = []

        for entry_num in range(0, myTree.GetEntries()):
            myTree.GetEntry(entry_num)

            if entry_num == 0:
                increment = myTree.dt
                n_points = myTree.NPoints
                v_bias = myTree.Vbias

            photodiode_current.append(float(myTree.IDiode))
            amplitude_tmp = np.array(list(myTree.Volt))

            if np.isnan(self.baseline_amplitude).any() == False:
                amplitude_tmp = amplitude_tmp - self.baseline_amplitude

            amplitude.append(amplitude_tmp)

            baseline = np.mean(amplitude_tmp)
            index_max.append(np.argmax(amplitude_tmp))

        amplitude = np.array(amplitude)

        photodiode_current = np.array(photodiode_current)
        photodiode_current = np.mean(photodiode_current)

        counts = np.bincount(index_max)
        most_probable = np.argmax(counts)
        most_probable_std = np.std(counts)

        if doshow:
            events, bins, patches = plt.hist(index_max, bins=n_points)
        else:
            events, bins = np.histogram(index_max, bins=n_points)

        x = np.arange(0, len(bins), 0.1)

        popt_gaus, pcov_gaus = curve_fit(self.gauss_function, bins[:-1], events,
                                         p0=[100., most_probable, most_probable_std], maxfev=10000)

        self.trigger_range = (int(popt_gaus[1] - abs(n_std * popt_gaus[2])), int(popt_gaus[1] + abs(n_std * popt_gaus[2])))

        if doshow:
            plt.plot(x, self.gauss_function(x, *popt_gaus), 'r-',
                     label='mean : {:.2e}, std : {:.2e}'.format(popt_gaus[1], popt_gaus[2]))

            plt.xlim(popt_gaus[1] - 7. * abs(popt_gaus[2]), popt_gaus[1] + 7. * abs(popt_gaus[2]))
            plt.plot([self.trigger_range[0], self.trigger_range[0]], [0, 1.1*np.max(events)], 'r-')
            plt.plot([self.trigger_range[1], self.trigger_range[1]], [0, 1.1 * np.max(events)], 'r-')
            plt.grid()
            plt.xlabel('time bin')
            plt.ylabel('n events')
            plt.legend()
            plt.show()
        # else:
        #    plt.clf()

        print('       - found trigger range : ', self.trigger_range)

        if any(np.isnan(self.nsb_range)):
            self.nsb_range = (0, int(0.5 * self.trigger_range[0]))
            print('       - nsb_range = ', self.nsb_range)
        else:
            print('       - nsb_range = ', self.nsb_range)

        print('   end: get_trigger_range')
        print('')

    def update_dictionary(self, my_dict, key, data):

        '''
        Add new key-value to existing dictionary
        :param my_dict: dictioary which should be updated
        :param key: key which should be added to dictionary
        :param data: value which should be added
        :return:
        '''

        try:
            tmp_list = my_dict[key]
            tmp_list.append(data)
        except KeyError:
            tmp_list = [data]

        my_dict[key] = tmp_list

        return my_dict

    def pde_fit(self, x, const, slope):
        '''
        Function to describe behavior of PDE vs. Overvoltage
        :param x:  Overvoltage
        :param const:  Max. PDE value
        :param slope: PDE slope
        :return:
        '''
        return const * (1. - np.exp(-(x) * slope))

    def fit_pde_data(self, x, y):
        '''
        Fit PDE vs. Overvoltage
        :param x: Overvoltage
        :param y: PDE
        :return:
        '''
        popt, pcov = curve_fit(self.pde_fit, x, y, p0=[50., 0.7], maxfev=10000)
        return popt

    def gauss_function(self, x, a, x0, sigma):
        '''
        Gaussian function
        :param x: x-data
        :param a: Amplitude
        :param x0: Mean
        :param sigma: Sigma
        :return:
        '''
        return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

    def line(self, x, a, b):
        '''
        Line function y = a + b*x
        '''
        return a + b * x

    def get_n_pe(self, f, r_area, current, current_err, ratio, QE, QE_err):

        '''
        Calculate number of photons reaching SiPM active area from Photodiode current
        :param f: LED repitition rate
        :param r_area: area ratio A_sipm/A_PD where A_sipm and A_PD are SiPM and Photodiode active areas respectively
        :param current: photodiode current
        :param current_err: photodiode current error
        :param ratio: $𝑅=𝑃_{𝑃𝐷}/𝑃_{𝑆𝑖𝑃𝑀}$, power ratio between the light intensity measured by the calibrated photodiode,
                        $𝑃_{𝑃𝐷}$, and the SiPM, $𝑃_{𝑆𝑖𝑃𝑀}$, is measured experimentally, by replasing the SiPM with another calibrated photodiode
        :param QE:      Photodiode Quantum efficency at a given wavelength
        :param QE_err:  Photodiode Quantum efficency error at a given wavelength
        :return:
        '''

        el = scipy.constants.e
        n_pe = current * ratio * r_area / (f * QE * el)

        n_pe_err_1 = current_err * ratio * r_area / (f * QE * el)
        n_pe_err_2 = current * ratio * r_area * QE_err / (f * QE * QE * el)
        n_pe_err = math.sqrt(n_pe_err_1*n_pe_err_1 + n_pe_err_2*n_pe_err_2)

        return n_pe, n_pe_err

    def ana_row_data(self, file, charge_window = (2495, 2510)):
        
        '''
        
        :param file: root file to analyse
        :param charge_window: time window for charge calculation (define in point numbers)
        :return: dictionary with data, where key is string of bias voltage
        '''

        print('   start: ana_row_data')
        print('       - file : ', file)
        print('')

        f = root.TFile(self.data_path + file)
        myTree = f.Get("T")

        n_points = 0
        increment = 0
        amplitude = []
        amplitude_arg_max = []
        amplitude_max = []
        baseline = []
        pulse_amplitude = []
        n_dcr = []
        n_light = []
        n_light_charge = []
        photodiode_current = []
        photodiode_current_std = []
        index_max = []

        # print('n events : ', myTree.GetEntries ())

        for entry_num in range(0, myTree.GetEntries()):
            myTree.GetEntry(entry_num)

            if entry_num == 0:
                increment = myTree.dt
                n_points = myTree.NPoints
                v_bias = myTree.Vbias

            photodiode_current.append(float(myTree.IDiode))
            photodiode_current_std.append(float(myTree.IDiodeStd))
            amplitude_tmp = np.array(list(myTree.Volt))

            # amplitude_tmp =  amplitude_tmp - np.roll(amplitude_tmp, 1)

            if np.isnan(self.baseline_amplitude).any() == False:
                amplitude_tmp = amplitude_tmp - self.baseline_amplitude

            # amplitude.append(list(myTree.Volt))

            amplitude.append(amplitude_tmp)

            baseline = np.mean(amplitude_tmp)
            # n_dcr.append(np.max(amplitude_tmp[1005:1015])-baseline)
            n_dcr.append(np.max(amplitude_tmp[self.nsb_range[0]:self.nsb_range[1]]) - baseline)
            n_light.append(np.max(amplitude_tmp[self.trigger_range[0]:self.trigger_range[1]]) - baseline)
            n_light_charge.append(np.sum(amplitude_tmp[charge_window[0]:charge_window[1]]) - len(amplitude_tmp[charge_window[0]:charge_window[1]]) * baseline)
            index_max.append(np.argmax(amplitude_tmp))
            # amplitude_arg_max.append( np.argmin(amplitude[entry_num]) )

        amplitude = np.array(amplitude)

        photodiode_current = np.abs(np.array(photodiode_current))
        photodiode_current = np.mean(photodiode_current)
        photodiode_current_std = np.mean(np.abs(photodiode_current_std))

        self.data[str(abs(v_bias))] = {'photdiode current': photodiode_current,
                                       'photdiode current std' : photodiode_current_std,
                                       'n light': n_light, 'n dcr': n_dcr,
                                       'index max': index_max, 'n points': n_points,
                                       'increment': increment, 'amplitude': amplitude,
                                       'charge': n_light_charge}

        # return amplitude, v_bias, n_points, increment, n_dcr, n_light, photodiode_current, index_max

        print('   end: ana_row_data')
        print('')
        
    def double_gauss_function(self, data, amplitude_0, gain, sigma_0, amplitude_1, sigma_1, shift):

        '''
        Double Gauss fit
        :param data: input data
        :param amplitude_0: first peak amplitude
        :param gain: device gain, i.e distance between peaks
        :param sigma_0: first peak STD
        :param amplitude_1: second peak amplitude
        :param sigma_1: second peak STD
        :param shift: horyzontal shift
        :return:
        '''

        return amplitude_0 * np.exp(-(data - gain - shift) ** 2 / (2 * sigma_0 ** 2)) + amplitude_1 * np.exp(-(data - 2.*gain - shift) ** 2 / (2 * sigma_1 ** 2))


    def ana_distribution(self, v_bias_str, key_data, fit_start_parameters, fit_range_parameters, threshold = np.nan, n_pe_fit = 2, doshow=False):
        
        '''
        Analyse the distributuin
        :param v_bias_str: data key, defined as string of bias voltage
        :param key_data: key for distribution, like 'n light', 'n dcr', etc.
        :param pe_aproximate: aproximate value of 1 p.e. amplitude
        :param threshold: threshold between 0 and 1 p.e., if nan -> threshold will be calculated
        :param n_pe_fit: number of peaks to be fitted
        :param doshow: show or not results
        :return: 
        '''

        print('   start: ana_distribution')
        print('       - Vbias         : ', v_bias_str)
        print('       - data name     : ', key_data)
        print('       - Threshold     : ', threshold)
        print('')

        n_data = self.data[v_bias_str][key_data]

        # amplitude_min = np.min(n_data)
        pe_aproximate = fit_start_parameters['gain']
        print('       - p.e. aproxim. : ', pe_aproximate)
        amplitude_min = 0.0
        #if np.min(n_data) < 0.:
        #    amplitude_min = np.min(n_data)
        #amplitude_max = 1. * np.max(n_data)
        #amplitude_max = 3. * np.mean(n_data)
        amplitude_max = 8.*fit_start_parameters['gain']

        test_data = self.data[v_bias_str]['amplitude'][0]
        bin_width = self.get_bin_size(test_data)
        nbins = int((amplitude_max - amplitude_min) / bin_width)
        nbins = 100
        if key_data == 'DCR pulses':
            nbins = int(0.5 * nbins)

        print('       - Bin size      : ', bin_width)
        print('       - N bins        : ', nbins)

        if doshow:
            events, bins, patches = plt.hist(n_data, bins=nbins, alpha=0.6, range=[amplitude_min, amplitude_max])
        else:
            events, bins = np.histogram(n_data, bins=nbins, range=[amplitude_min, amplitude_max])

        if np.isnan(pe_aproximate):
            pe_aproximate = bins[np.argmax(events[int(0.35 * len(events)):])]

        if pe_aproximate < 0.0:
            pe_aproximate = 0.01

        print('       - pe_aproximate : ', pe_aproximate)

        pe_calc = pe_aproximate

        if np.isnan(threshold):

            distance_peaks = 0.7 * pe_aproximate / (bins[1] - bins[0])
            print('       - distance_peaks: ', distance_peaks)

            if distance_peaks <= 1:
                distance_peaks = 4
                print('       - distance_peaks: ', distance_peaks)

            peaks, _ = find_peaks(events, height=50, distance=distance_peaks)

            if key_data == 'DCR pulses' or len(peaks < 1):
                peaks, _ = find_peaks(events, height=10, distance=distance_peaks)

            if len(peaks) > 1 :
                pe_calc = (peaks[1] - peaks[0]) * (bins[1] - bins[0])
            else:
                pe_calc = 1.4*peaks[0]
                peaks, _ = find_peaks(events, height=10, distance=int(0.2*nbins))
                plt.plot(bins[peaks], events[peaks], "o")

            print("       - found : ", len(peaks))
            print("       - pe_calc : ", pe_calc)

            distance_peaks = 0.9 * pe_calc / (bins[1] - bins[0])
            if distance_peaks <= 1:
                distance_peaks = 4

            # peaks, _ = find_peaks(events, height=10, distance=distance_peaks)

            # peaks = peaks[:3]
            print("       - found : ", len(peaks))

            if doshow:
                plt.plot(bins[peaks], events[peaks], "rx")

            if len(peaks) > 1:

                if len(peaks) > n_pe_fit:
                    n_read_peaks = n_pe_fit
                else:
                    n_read_peaks = len(peaks)

                pe_calc = (peaks[1] - peaks[0]) * (bins[1] - bins[0])
                print('       - p.e. : ', pe_calc)

                x = np.arange(0.0, amplitude_max, 0.00001)
                popt = []

                # for i_peak in range(len(peaks)):
                for i_peak in range(n_read_peaks):

                    # xfit_tmp = bins[:-1]
                    # xfit = xfit_tmp[xfit_tmp < bins[peaks[i_peak]] + 0.5*(bins[peaks[i_peak+1]] - bins[peaks[i_peak]])]

                    if i_peak < len(peaks) - 1:

                        # right_element = len(bins[bins < bins[peaks[i_peak]] + 0.5*(bins[peaks[i_peak+1]] - bins[peaks[i_peak]]) ])
                        right_element = len(bins[bins < bins[peaks[i_peak]] + 0.35 * pe_calc])
                    else:
                        right_element = len(bins) - 1

                    if (i_peak == 0):
                        left_element = 0

                    # yfit = events[xfit_tmp < bins[peaks[i_peak]] + 0.5*(bins[peaks[i_peak+1]] - bins[peaks[i_peak]])]
                    xfit = bins[left_element:right_element]
                    yfit = events[left_element:right_element]

                    print('       - left_element : ', bins[left_element], ' right_element : ', bins[right_element])

                    #popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit,
                    #                               p0=[events[peaks[i_peak]], bins[peaks[i_peak]], 0.005],
                    #                               maxfev=1000000)
                    if i_peak == 0:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit, p0=[events[peaks[i_peak]], bins[peaks[i_peak]], 1e-5], maxfev=1000000)

                    elif popt_tmp[1] > 0.0:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit,
                                                       p0=[events[peaks[i_peak]], 2.*popt_tmp[1], popt_tmp[2]],
                                                       maxfev=1000000)
                    else:
                        popt_tmp, pcov_tmp = curve_fit(self.gauss_function, xfit, yfit,
                                                       p0=[events[peaks[i_peak]], bins[peaks[i_peak]], popt_tmp[2]],
                                                       maxfev=1000000)
                    print('       - fit results : ', popt_tmp)

                    #if doshow:
                    #    plt.plot(x, self.gauss_function(x, *popt_tmp), 'r-',
                    #             label='fit {:}, mean = {:.2e}, std = {:.2e}'.format(i_peak, popt_tmp[1], popt_tmp[2]))

                    popt.append(popt_tmp)
                    left_element = right_element
                    
                amplitude_0 = fit_start_parameters['amplitude_0']
                amplitude_0_range = fit_range_parameters['amplitude_0']

                #gain = popt[0][1]
                gain = fit_start_parameters['gain']
                gain_range = fit_range_parameters['gain']

                #sigma_0 = popt[0][2]
                sigma_0 = fit_start_parameters['sigma_0']
                sigma_0_range = fit_range_parameters['sigma_0']

                amplitude_1 = fit_start_parameters['amplitude_1']
                amplitude_1_range = fit_range_parameters['amplitude_1']

                sigma_1 =fit_start_parameters['sigma_1']
                #sigma_1 = 2. * popt[0][2]
                sigma_1_range = fit_range_parameters['sigma_1']

                shift = fit_start_parameters['shift']
                shift_range = fit_range_parameters['shift']
                #shift_range = (-0.3 * np.abs(popt[0][1]), 0.3 * np.abs(popt[0][1]))

                my_par_range_min = (amplitude_0_range[0], gain_range[0], sigma_0_range[0], amplitude_1_range[0], sigma_1_range[0], shift_range[0])
                my_par_range_max = (amplitude_0_range[1], gain_range[1], sigma_0_range[1], amplitude_1_range[1], sigma_1_range[1], shift_range[1])

                my_range = (my_par_range_min, my_par_range_max)
                start_p = [amplitude_0, gain, sigma_0, amplitude_1, sigma_1, shift]

                #a_cut = 3.5 * popt[1][1]
                #a_cut = 1.5 * popt[1][1]
                #a_cut = 0.5*np.mean(n_data)
                #a_cut = 1.5*bins[peaks[1]]
                a_cut = bins[peaks[1]] + 0.55*(bins[peaks[1]] - bins[peaks[0]])
                #print('1.5*peaks[1] : ', 1.5*peaks[1])
                #print(' a cut : ',a_cut)
                if bins[-1] > a_cut:
                    x_fit = bins[bins<a_cut]
                    y_fit = events[bins[:-1]<a_cut]
                else:
                    x_fit = bins[:-1]
                    y_fit = events
                    
                #print('x_fit [-1] : ', x_fit[-1])

                popt_double, pcov_double = curve_fit(self.double_gauss_function, x_fit, y_fit, p0=start_p,  bounds=my_range, maxfev=1000000)
                print('       - fit parameters : ')
                print('            - amplitude_0 : ', popt_double[0])
                print('            - gain        : ', popt_double[1])
                print('            - sigma_0     : ', popt_double[2])
                print('            - amplitude_1 : ', popt_double[3])
                print('            - sigma_1     : ', popt_double[4])
                print('            - shift       : ', popt_double[5])
                
                if doshow:
                    plt.plot(x_fit, self.double_gauss_function(x_fit, *popt_double), 'r-', linewidth=2.5, label = 'fit, gain = {:.1e}, shift = {:.1e}'.format(popt_double[1], popt_double[5]))
                    if key_data != 'n dcr':
                        plt.plot(x_fit, self.double_gauss_function(x_fit, popt_double[0], popt_double[1], popt_double[2], 0, popt_double[4], popt_double[5]), '--', label = '1st peak fit, std = {:.1e}'.format(popt_double[2]))
                        plt.plot(x_fit, self.double_gauss_function(x_fit, 0, popt_double[1], popt_double[2], popt_double[3], popt_double[4], popt_double[5]), '--', label = '2nd peak fit, std = {:.1e}'.format(popt_double[4]) )

                #threshold = popt[0][1] + 0.4 * (popt[1][1] - popt[0][1])
                threshold = popt_double[5] + 1.5*popt_double[1]
                pe_calc = popt[1][1] - popt[0][1]
                self.data[v_bias_str][key_data + str(' ana')] = {'pe': pe_calc, 
                                                                 'threshold': threshold,
                                                                 'fit data' : popt_double}

        print('       - threshold : ', threshold)
        threshold_element = len(bins[bins < threshold])
        print('       - threshold element N : ', threshold_element)

        n0 = np.sum(events[:threshold_element])
        n0_err = math.sqrt(n0)
        n_total = np.sum(events)
        n_total_err = math.sqrt(n_total)

        p0 = n0 / np.sum(events)
        p0_err = math.sqrt( math.pow(n0_err/n_total, 2) + math.pow((n_total_err*n0)/(n_total*n_total), 2))
        print('       - n_0   {:}, with err {:.2f} '.format(n0, n0_err))
        print('       - n_all {:}, with err {:.2f} '.format(n_total, n_total_err))
        print('       - P0    {:.2f}, with err {:.2f} '.format(p0, p0_err))

        if key_data == 'n dcr':
            self.data[v_bias_str]['DCR Poisson'] = -math.log(p0) / (
                        self.data[v_bias_str]['increment'] * (self.nsb_range[1] - self.nsb_range[0]))
            print('       - DCR Poisson', self.data[v_bias_str]['DCR Poisson'])

        if doshow:
            plt.plot([threshold, threshold], [0.0, 1.2*np.max(events)], '--r')
            plt.legend()
            plt.title('Data : {:}, Vbias : {:}'.format(key_data, v_bias_str))
            plt.xlabel("Amplitude, V")
            plt.ylabel("N events")
            plt.grid()
            plt.ylim(1, 1.1*np.max(events))
            plt.xlim(amplitude_min, amplitude_max)
            # plt.yscale('log')
            plt.show()

        print('   end: ana_distribution')
        print('')

        return n0, n_total, pe_calc, threshold

    def get_bin_size(self, data):
        
        '''
        get correct distribution bin size
        :param data: data for which the distibution should be built
        :return: correct bin size
        '''

        counts, bins = np.histogram(data, bins=1000, range=[0, 0.01])
        max_index = np.argmax(counts)

        found = False
        index = max_index + 1

        while found == False:
            if counts[index] > 0:
                found = True
            else:
                index = index + 1

        return bins[index] - bins[max_index]

    def get_qe(self, path_qe, qe_err = 5):

        '''

        :param path_qe: file with QE data for calibrated Photodiode
        :return: get QE at a given wavelength and interpolation function "f_qe"
        '''

        self.path_qe = path_qe
        qe_data = np.loadtxt(self.path_qe, dtype=float, comments='#', delimiter=' ')
        self.wl_qe = qe_data[:, 0]
        self.qe = qe_data[:, 1]
        self.f_qe = interp1d(self.wl_qe, self.qe, kind='cubic')

    def calculate_ratio_file(self, file_ratio):

        '''
        calculate 𝑅 - is the power ratio, $𝑅=𝑃_{𝑃𝐷}/𝑃_{𝑆𝑖𝑃𝑀}$,
        between the light intensity measured by the calibrated photodiode,
        $𝑃_{𝑃𝐷}$, and the SiPM, $𝑃_{𝑆𝑖𝑃𝑀}$, is measured experimentally,
        by replasing the SiPM with another calibrated photodiode
        :param file_ratio: -> file with R measuremetns
        :return:
        '''

        self.ratio_data = np.loadtxt(file_ratio, dtype=float, comments='#', delimiter = '	')
        self.power_r = np.abs(np.mean(self.ratio_data[:,4]))


    def calc_pde(self, v_bias_str, fit_start_parameters, fit_range_parameters, r_area, qe_file, ratio_file, led_wl, frequency = 1000, qe_err = 5, doshow = False):

        '''
        Calculate PDE

        :param v_bias_str: data key -> string of Vbias
        :param pe_aproximate: aproximate 1 p.e. amplitude
        :param r_area: ratio between SiPM (A_SiPM) and Photodiode (A_PD) active areas r_area = A_SiPM/A_PD
        :param qe_file: file with Photodiode QE
        :param ratio_file: wile with power ratio $𝑅=𝑃_{𝑃𝐷}/𝑃_{𝑆𝑖𝑃𝑀}$,
                            between the light intensity measured by the calibrated photodiode,
                            $𝑃_{𝑃𝐷}$, and the SiPM, $𝑃_{𝑆𝑖𝑃𝑀}$
        :param led_wl: LED wavelength
        :param frequency: LED repition rate
        :param qe_err: Photodiode QE error
        :param doshow: show or not results
        :return:
        '''

        # analyse the distribution to get n0 for light and dark:
        n0_light, n_total_light, pe_calc, threshold = self.ana_distribution(v_bias_str, 'n light', fit_start_parameters, fit_range_parameters, np.nan, doshow = doshow)
        p0_light = n0_light / n_total_light
        n0_dark, n_total_dark, pe_calc_dark, threshold_dark = self.ana_distribution(v_bias_str, 'n dcr', fit_start_parameters, fit_range_parameters, threshold, doshow=doshow)
        p0_dark = n0_dark / n_total_dark

        time_light = self.trigger_range[1] - self.trigger_range[0]
        time_dark = self.nsb_range[1] - self.nsb_range[0]
        #if time interval for light and dark are not equavalent corret the p0 dark:
        p0_dark_corrected = 1. - time_light*(1. - p0_dark)/time_dark

        #calculate the average number of photons detected by SiPM and its errors:
        k_LED = -math.log(p0_light) + math.log(p0_dark_corrected)

        k_LED_light_err = math.sqrt(n0_light * (1. - n0_light / n_total_light)) / n0_light
        k_LED_dark_err = math.sqrt(n0_dark * (1. - n0_dark / n_total_dark)) / n0_dark
        k_LED_err = math.sqrt(k_LED_light_err * k_LED_light_err + k_LED_dark_err * k_LED_dark_err)

        DCR = -math.log(p0_dark) / ((self.nsb_range[1] - self.nsb_range[0]) * self.baseline_increment)

        # get QE and power R
        self.get_qe(qe_file)
        self.calculate_ratio_file(ratio_file)

        # calculate the average number of photons reaching the SiPM active area and its error:
        n_pe, n_pe_err = self.get_n_pe(frequency, r_area, self.data[v_bias_str]['photdiode current'], self.data[v_bias_str]['photdiode current std'],
                                        self.power_r, 0.01*self.f_qe(led_wl), 0.01*qe_err*0.01*self.f_qe(led_wl))

        # calcualte PDE and its error:
        pde = 100*k_LED/n_pe
        pde_err = 100 * math.sqrt((k_LED_err / n_pe) * (k_LED_err / n_pe) + ((n_pe_err * k_LED) / (n_pe * n_pe)) * (
                    (n_pe_err * k_LED) / (n_pe * n_pe)))


        print('k LED {:.2f}, err {:.2f} '.format(k_LED, k_LED_err))
        print('n pe  {:.2f}, err {:.2f} '.format(n_pe, n_pe_err))
        print ('PDE  {:.2f}, err {:.2f} '.format(pde, pde_err))

        # Updata data dictionary
        self.update_dictionary(self.data[v_bias_str], 'p.e. amplitude', pe_calc)
        self.update_dictionary(self.data[v_bias_str], 'k LED', k_LED)
        self.update_dictionary(self.data[v_bias_str], 'k LED err', k_LED_err)
        self.update_dictionary(self.data[v_bias_str], 'n pe', n_pe)
        self.update_dictionary(self.data[v_bias_str], 'n pe err', n_pe_err)
        self.update_dictionary(self.data[v_bias_str], 'PDE', pde)
        self.update_dictionary(self.data[v_bias_str], 'PDE err', pde_err)
